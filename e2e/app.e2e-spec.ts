import { PmFrontPage } from './app.po';

describe('pm-front App', () => {
  let page: PmFrontPage;

  beforeEach(() => {
    page = new PmFrontPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
