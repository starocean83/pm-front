import { Component } from '@angular/core';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})

export class AppComponent{
	user: any;
	constructor() {
		if(localStorage.getItem('currentUser')){
			this.user = JSON.parse(localStorage.getItem('currentUser'));
		}
	}
	checkLogin(): Boolean {
		var a = false;
		if(this.user){
			a = true;
		}
		return a;
	}
}
