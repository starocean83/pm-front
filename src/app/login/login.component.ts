import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { LoginService } from '../services/login.service';
import { FormBuilder, FormGroup, Validators  } from '@angular/forms';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
	loginForm: FormGroup;
	constructor(private router: Router, private ls: LoginService,
		private activatedRoute: ActivatedRoute, private fb: FormBuilder) {
		if (localStorage.getItem('currentUser')) {
			this.router.navigate(['/dashboard']);
		}
		this.loginForm = fb.group({
			// 定義表格的預設值
			'account': [null, Validators.required],
			'password': [null, Validators.required],
			'remember': true,
		})
	}
	returnUrl: string = "/dashboard";
	ngOnInit() {
		this.activatedRoute.queryParams.subscribe(params => {
			let url = params['returnUrl'];
			if (url) {
				this.returnUrl = url;
			}
		});
		$('body').addClass("login");
	}
	ngOnDestroy() {
		$('body').removeClass("login");
	}
	login() {
		var isLogin = this.ls.login();
		if (isLogin) {
			$('body').removeClass("login");
			this.router.navigate([this.returnUrl]);
		}
	}
}
