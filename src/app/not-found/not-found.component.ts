import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
	selector: 'app-not-found',
	templateUrl: './not-found.component.html',
	styleUrls: ['./not-found.component.css']
})
export class NotFoundComponent implements OnInit, OnDestroy {

	constructor() { }

	ngOnInit() {
		$('body').addClass("page-404-full-page");
	}

	ngOnDestroy() {
		$('body').removeClass("page-404-full-page");
	}
}
