import { Component, OnInit } from '@angular/core';
declare var Layout;  //layout.js
@Component({
	selector: 'dashboard-cmp',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

	constructor() { }

	ngOnInit() {
		Layout.init();
	}

}
