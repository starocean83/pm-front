import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../../../services/login.service';

@Component({
	selector: 'header-cmp',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
	user: any;
	constructor(private ls: LoginService, private router: Router) {
		ls.gUserObs.subscribe(user =>{
			this.user = user;
		});

	}

	ngOnInit() {
		this.user = this.ls.gUser;
	}

	logout(){
		this.ls.logout();
		this.router.navigate(["/login"]);
	}
}
