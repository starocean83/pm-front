import { Component, OnInit, ViewChildren, ElementRef, QueryList } from '@angular/core';
import { Func } from '../../models/func';
import { Router, NavigationEnd } from '@angular/router';
import { Http } from '@angular/http';

declare var App;  //app.js

@Component({
	selector: 'sidebar-cmp',
	templateUrl: './sidebar.component.html',
	styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
	@ViewChildren('mainmenu') mainMenu: QueryList<ElementRef>;

	constructor(private http: Http, private router: Router) {
		this.http.get('assets/menu.json').subscribe(res => {
			let tmp = res.json();
			tmp.forEach(element => {
				element.isOpen = false;
				if (element.childs.find(child => router.url.indexOf(child.link) >= 0 )) {
					element.isOpen = true;
				}
			});
			this.funcList = tmp;
			App.unblockUI($("#sidebarDiv"));
		});
		router.events.subscribe(val => {
			if (val instanceof NavigationEnd) {
				this.funcList.forEach(element => {
					element.isOpen = false;
					let c = element.childs.find(child => val.url.indexOf(child.link) >= 0);
					if (c) {
						element.isOpen = true;
						let mid = "menu" + element.name;
						let menu = this.mainMenu.find(menu => menu.nativeElement.id == mid);
						if(!(menu.nativeElement.parentElement.className.indexOf("open") >= 0))
						{
							menu.nativeElement.click();
						}
					}
				});
			}
		});
	}
	funcList: Func[] = [];
	ngOnInit() {
		App.blockUI({
			target: $("#sidebarDiv"),
			animate: true
		});
	}
}
