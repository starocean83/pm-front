import { Route, RouterModule } from '@angular/router';
import { ModuleWithProviders} from '@angular/core';
import { SidebarComponent } from "./sidebar/sidebar.component";
import { HomeComponent } from './home/home.component';
import { AccountComponent } from './account/account.component';
import { DashboardComponent } from './dashboard.component';
import { AuthGuard } from '../guards/auth.guard';

const route: Route[] =[
	{path:'', canActivateChild:[AuthGuard],component:DashboardComponent,children:[
		{path: '', redirectTo:'/dashboard/home',pathMatch:'full'},
		{path: 'home',component: HomeComponent},
		{path: 'account',component: AccountComponent}
	]}

]
export const MODULE_ROUTES = RouterModule.forChild(route);
export const MODULE_COMPONENTS = [
    HomeComponent,
	SidebarComponent,
	AccountComponent,
	DashboardComponent
]
