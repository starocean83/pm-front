import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MODULE_COMPONENTS, MODULE_ROUTES } from './dashboard.routes';
import { HeaderComponent } from './shared/header/header.component';


@NgModule({
    imports: [
        MODULE_ROUTES,
		CommonModule
    ],
    declarations: [ MODULE_COMPONENTS, HeaderComponent ]
})

export class DashboardModule{}
