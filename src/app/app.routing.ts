import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AuthGuard } from './guards/auth.guard';

const appRoutes: Routes = [
	{
		path: '', canActivate: [AuthGuard], children: [
			{ path: '', redirectTo: "/dashboard/home", pathMatch: 'full' },
			{
				path: 'dashboard', loadChildren:"app/dashboard/dashboard.module#DashboardModule"
			}
		]
	},
	{ path: 'login', component: LoginComponent },
	{ path: '404', component: NotFoundComponent},
	// otherwise redirect to home
	{ path: '**', redirectTo: '404' }
];
export const routing = RouterModule.forRoot(appRoutes);
