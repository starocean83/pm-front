export class Func {
	id: number;
	name: string;
	link: string;
	childs: Func[];
	icon_class: string;
	isOpen: boolean;
}
