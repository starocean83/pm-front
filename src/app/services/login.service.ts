import { Injectable } from '@angular/core';
import { User } from '../models/user'
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class LoginService {
	private subject = new Subject<any>();
	gUser: User;
	gUserObs = this.subject.asObservable();
	constructor() {
		if (localStorage.getItem('currentUser')) {
			this.gUser = JSON.parse(localStorage.getItem('currentUser'));
			this.subject.next(this.gUser);
		} else {
			this.logout();
		}
	}

	login(): Boolean {
		this.gUser = new User();
		this.gUser.id = 1;
		this.gUser.firstName = "Gordon";
		this.gUser.lastName = "Tseng";
		this.gUser.username = "Gordon Tseng";
		localStorage.setItem('currentUser', JSON.stringify(this.gUser));
		this.subject.next(this.gUser);
		return true;
	}

	logout() {
		localStorage.removeItem('currentUser');
		this.gUser = null;
		this.subject.next();
	}
}
