import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { routing } from './app.routing';

import { AuthGuard } from './guards/auth.guard';
import { LoginService } from './services/login.service';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { User } from './models/user';
import { Func } from './models/func';
import { NotFoundComponent } from './not-found/not-found.component';


@NgModule({
	declarations: [
		AppComponent,
		LoginComponent,
		NotFoundComponent,
	],
	imports: [
		BrowserModule,
		FormsModule,
		ReactiveFormsModule,
		HttpModule,
		routing
	],
	providers: [AuthGuard, User, LoginService, Func],
	bootstrap: [AppComponent]
})
export class AppModule { }
